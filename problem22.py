#! /usr/bin/env python
#-*- coding:utf-8 -*-
#
# Problem 22
#
# April 20, 2012
#
# Using names.txt (right click and 'Save Link/Target As...'), a 46K text
# file containing over five-thousand first names, begin by sorting it into
# alphabetical order. Then working out the alphabetical value for each
# name, multiply this value by its alphabetical position in the list to
# obtain a name score.
# 
# For example, when the list is sorted into alphabetical order, COLIN,
# which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the
# list. So, COLIN would obtain a score of 938 �� 53 = 49714.
# 
# What is the total of all the name scores in the file?
#
# Answer: 871198282
#
# Annoying bug: have to include encoding format in header

def QuickSort( list, start, end ):
    n = end-start
    if n==1:
        return 0
    pivot = ChoosePivot( list, start, end, 1 )  # 1: first element
				   # 2: last element
                                                                   # 3: median element
    boundary = Partition( list, start, end )  # partition around pivot
    one = 0
    two = 0
    if start != boundary:
        one = QuickSort( list, start, boundary )
    if boundary != end -1:
        two = QuickSort( list, boundary+1, end )
    return one+two+n-1

def ChoosePivot( list, start, end, choice ):
    if choice == 1:
        return list[start]
    if choice == 2:
        list[start], list[end-1] = list[end-1], list[start]
        return list[start]
    if choice == 3:
        n = end-start
        if n % 2 == 1:
            middle = n/2+start
        else:
            middle = n/2-1+start
        top = max( list[start], list[end-1], list[middle] )
        bot = min( list[start], list[end-1], list[middle] )
        if list[middle] != top and list[middle] != bot:
            list[start], list[middle] = list[middle], list[start]
        elif list[end-1] != top and list[end-1] != bot:
            list[start], list[end-1] = list[end-1], list[start]
        return list[start]

def Partition( list, start, end ):
    pivot = list[start]
    i = start+1
    for j in range( start+1, end ):
        if list[j] < pivot:
            list[j], list[i] = list[i], list[j]
            i += 1
    list[start], list[i-1] = list[i-1], list[start]
    return i-1

def get_next_name( page ):
    start_quote = page.find('"')
    if start_quote == -1:
        return None, 0
    end_quote = page.find('"', start_quote + 1)
    name = page[start_quote + 1:end_quote]
    return name, end_quote+1

def get_names( page ):
    names = []
    count = 0
    while True:
        name,endpos = get_next_name(page)
        if name:
            count += 1
            names.append(name)
            page = page[endpos:]
        else:
            break
    return names, count

filename = "problem22.txt"
FILE = open( filename, "r" )
page = FILE.readline()
list = []
list, count = get_names( page )
FILE.close()
#print list[0:10]
QuickSort( list, 0, count)
#print list[0:10]

sum = 0
for i in range(0, count):
    score = 0
    for j in range(0, len(list[i])):
        score += ord(list[i][j]) - 64
    sum += score*(i+1)

print sum